
#include "i2c.h"

#define SENSOR_ADDR 0x1A
#define EEPROM_ADDR 0x1B

//Function to obtain sensor data
int b[780];

void acquire()
{
    i2c_t i2c;
    int j = 0;
    /* Open the i2c-0 bus */
    if (i2c_open(&i2c, "/dev/i2c-1") < 0) 
    {
        fprintf(stderr, "i2c_open(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    else
    {
        printf("I2C Port Opened\n") ;
    }

    /* Wakeup Sensor */
    
    uint8_t msg_data[2] = { 0x01, 0x01 };
    struct i2c_msg msgs[1] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 2, .buf = (char*) msg_data } ,
           
        };

    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    else
    {
        printf ("Sensor Awake\n") ;
    }
  
   /* Writing caliberation values onto the sensor */
    printf("writing caliberation values \n");
    uint8_t msg_addr_mbit[2] = { 0x03, 0x2C };
    struct i2c_msg msgscalib[1] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 2, .buf = (char*) msg_addr_mbit },
           
        };
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }


    uint8_t msg_addr_bias1[2] = { 0x04, 0x05 };
    msgscalib->buf = (char*) msg_addr_bias1;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    uint8_t msg_addr_bias2[2] = { 0x05, 0x05 };
    msgscalib->buf = (char*) msg_addr_bias2;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    uint8_t msg_addr_clk[2] = { 0x06, 0x15 };
    msgscalib->buf = (char*) msg_addr_clk;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    uint8_t msg_addr_bpa1[2] = { 0x07, 0x0C };
    msgscalib->buf = (char*) msg_addr_bpa1;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    uint8_t msg_addr_bpa2[2] = { 0x08, 0x0C };
    msgscalib->buf = (char*) msg_addr_bpa2;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    uint8_t msg_addr_pu[2] = { 0x09, 0x88 };
    msgscalib->buf = (char*) msg_addr_pu;
    if (i2c_transfer(&i2c, msgscalib, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    

    /* Waking up the sensor*/
    uint8_t msg_addr_sen[2] = { 0x01, 0x01 };
    struct i2c_msg msgs_sen[1] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 2, .buf = (char*) msg_addr_sen },
           
        };
    if (i2c_transfer(&i2c, msgs_sen, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    
    usleep(10000);
    /*Reading sensor values Block 0*/
    uint8_t msg_addr_blk[2] = { 0x01, 0x09 };
    struct i2c_msg msgs_blk[1] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 2, .buf = (char*) msg_addr_blk },
           
        };
    if (i2c_transfer(&i2c, msgs_blk, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    printf("reached point 1");
    usleep(200000);

    uint8_t msg_addr_status[1] = { 0x02 };
    uint8_t msg_data_status[1] = {};
    struct i2c_msg msgs_status[2] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 1, .buf = (char*) msg_addr_status },
           
           /* Read 8-bit data */
            { .addr = SENSOR_ADDR, .flags = I2C_M_RD, .len = 1, .buf = (char*)msg_data_status},
        };

    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_status, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    /*Reading top block*/
    uint8_t msg_addr_read[1] = { 0x0A };
    uint8_t msg_data_read[130] = {};
    struct i2c_msg msgs_read[2] =
        {
            { .addr = SENSOR_ADDR, .flags = 0, .len = 1, .buf = (char*) msg_addr_read },
            { .addr = SENSOR_ADDR, .flags = I2C_M_RD, .len = 130, .buf = (char*)msg_data_read},
        };

    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];

    /*Rading bottom block*/
    msg_addr_read[0] = { 0x0B };
    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];


    /*Reading sensor values Block 1*/
    msg_addr_blk[1] = 0x19; 
    if (i2c_transfer(&i2c, msgs_blk, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    usleep(200000);

    msg_addr_status[0] =  0x02 ; 
    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_status, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    /*Reading top block*/
    msg_addr_read[0] =  0x0A ;
    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];

    /*Rading bottom block*/
    msg_addr_read[0] = { 0x0B };
    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];


    /*Electrical Offsets*/
    msg_addr_blk[1] = 0xF; 
    if (i2c_transfer(&i2c, msgs_blk, 1) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }
    usleep(200000);

    msg_addr_status[0] =  0x02 ; 
    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_status, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    /*Reading top block*/
    msg_addr_read[0] =  0x0A ;
    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];

    /*Rading bottom block*/
    msg_addr_read[0] = { 0x0B };
    /* Transfer a transaction with two I2C messages */
    if (i2c_transfer(&i2c, msgs_read, 2) < 0) {
        fprintf(stderr, "i2c_transfer(): %s\n", i2c_errmsg(&i2c));
        exit(1);
    }

    for (int i=0;i<130;i++,j++)
        b[j] = msg_data_read[i];

}
