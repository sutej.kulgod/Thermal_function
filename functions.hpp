#include<iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "sensor.hpp"
#include "constants.hpp"
#include "lookup.hpp"
#include <unistd.h>


//function for concatinating two bytes
int lend(int a,int b)
{	int x;
	b=b<<8;
	x = a | b;
	return x;
}

//function to find the sum of all the elements in a matrix
int sum(int* a,int size)
{	int i;
	int sum=0;
	size = size/sizeof(int);
	for(i=0;i<size;i++)
		sum = sum+a[i];
	return sum;
}

//function to return sensor data
int V[16][16],PTAT[4],VDD[2],eloffset[16][16];

void sensor(int* b){
	int i,j;
	int v1[16][32],eloffset_1[8][16],el[8][32];
	int rowt=0;
	int rowb=15;
	int column=0;
	for(i=0;i<520;i++){
		if(i%130==0)
			PTAT[int(i/130)] = lend(b[i+1],b[i]);
		if((i>1 && i<130)||(i>261 && i<390)){
			v1[rowt][column]=b[i];
			column = column+1;
			if(column>31){
				column = 0;
				rowt = rowt+1;
			}
		}
		if ((i>131 && i<260) || (i>391 && i<520)){
			v1[rowb][column]=b[i];
			column = column+1;
			if(column >31){
				column = 0;
				rowb = rowb-1;
			}
		}
	}
	for(i=0;i<16;i++)
		for(j=0;j<32;j=j+2)
			V[i][j/2]= lend(v1[i][j+1],v1[i][j]);

	rowt=0;
	rowb=7;
	column=0;

	for(i=520;i<780;i++){
		if(i%130==0)
			VDD[int((i-520)/130)] = lend(b[i+1],b[i]);
		if (i>521 && i<650){
			el[rowt][column]=b[i];
			column = column+1;
			if (column >31){
				column = 0;
				rowt = rowt+1;
			}
		}
		if (i>651 && i<780){
			el[rowb][column]=b[i];
			column = column+1;
			if(column >31){
				column = 0;
				rowb = rowb-1;
			}
		}
	}
	for(i=0;i<8;i++)
		for(j=0;j<32;j=j+2)
			eloffset_1[i][j/2]= lend(el[i][j+1],el[i][j]);

	for(i=0;i<16;i++){
		for(j=0;j<16;j++){
			if(i<4)
				eloffset[i][j] = eloffset_1[i][j];
			else if(i<8)
				eloffset[i][j] = eloffset_1[i-4][j];
			else if(i<12)
				eloffset[i][j] = eloffset_1[i-4][j];
			else if(i<16)
				eloffset[i][j] = eloffset_1[i-8][j];
		}
	}

}

//Lookup function
extern int Lookuptab[][8];
float lookup(double V_PixC_1, int Ta)
{
	int i,j,x,y;
	float x_1,x_2,Temp_2,Temp_1;
	for(i=0;i<1595;i++)
		if((Lookuptab[i][0]<V_PixC_1) && (Lookuptab[i+1][0] > V_PixC_1))
			x=i;
	for (j=0;j<8;j++)
		if ((Lookuptab[0][j] < Ta) && (Lookuptab[0][j+1] > Ta))
			y=j;
	//Interpolating
	x_1 = (V_PixC_1 - Lookuptab[x][0])*(Lookuptab[x+1][y]-Lookuptab[x][y])/(Lookuptab[x+1][0]-Lookuptab[x][0]) + Lookuptab[x][y];
	x_2 = (V_PixC_1 - Lookuptab[x][0])*(Lookuptab[x+1][y+1]-Lookuptab[x][y+1])/(Lookuptab[x+1][0]-Lookuptab[x][0]) + Lookuptab[x][y+1];
	Temp_2 = float(Ta - Lookuptab[0][y])/(Lookuptab[0][y+1] - Lookuptab[0][y])*(x_2-x_1) + x_1;
	Temp_1 = Temp_2/10-273.14;
	return Temp_1;
}

//Main calculation
float temperature()
{       clock_t tstart,tend;
        tstart =clock();
	int i,j;
	        //Getting sensor data assigned to b
	acquire();

	sensor(b);
	//ambient temperature calculation
	float PTAT_av = sum(PTAT,sizeof(PTAT))/4.0;
	float Ta=PTAT_av*PTAT_grad+PTAT_offset;
	
	//thermal offset 
	double V_Comp[16][16],V_Comp_2[16][16],V_Comp_3[16][16],PixC[16][16],V_PixC[16][16],Temp[16][16];
	for(i=0;i<16;i++)
		for(j=0;j<16;j++)
			V_Comp[i][j]=V[i][j]-(ThGrad[i][j]*PTAT_av)/(pow(2.0,gradscale))-ThOffset[i][j];
	
	//Electrical Offset
	for(i=0;i<16;i++)
		for(j=0;j<16;j++)
			V_Comp_2[i][j]=V_Comp[i][j]-eloffset[i][j];

	
	//VDD Compensation
	float VDD_av = sum(VDD,sizeof(VDD))/2.0;
	for(i=0;i<16;i++)
		for(j=0;j<16;j++)
			V_Comp_3[i][j] = V_Comp_2[i][j] - (((((VddCompGrad[i][j]*PTAT_av)/pow(2.0,VddScGrad)) + VddCompOff[i][j])/pow(2.0,VddScOff))*(VDD_av - Vdd_th1 - ((Vdd_th2 - Vdd_th1)/(PTAT_th2-PTAT_th1))*(PTAT_av-PTAT_th1)));

	//Object Temperature
	for(i=0;i<16;i++){
		for(j=0;j<16;j++){
			PixC[i][j]=(P[i][j]*(PixC_max-PixC_min)/65535+PixC_min)*epsilon/100*GlobalGain/10000;
			V_PixC[i][j]=V_Comp_3[i][j]*PCSCALEVAL/PixC[i][j];
		}
	}
	
	//Lookup Table and final temperature matrix
	int maxt = Temp[0][0];
	int mint = 100;
	for(i=0;i<16;i++){
		for(j=0;j<16;j++){
			Temp[i][j] = lookup(V_PixC[i][j],Ta);
			if (Temp[i][j]>maxt)
				maxt = Temp[i][j];
			else if (Temp[i][j]<mint)
				mint = Temp[i][j];
		}
	}
	std::cout<<maxt<<"!!"<<mint<<std::endl;
	//Opencv Functions
	//Opencv Functions
	cv::Mat temp_filt,temp_bw = cv::Mat(16, 16, CV_64F) ,temp_fin, img, img_gray;
    cv::Mat img_1 = cv::Mat(16, 16, CV_64F, Temp);
    cv::normalize(img_1, img_gray, 1, 0, cv::NORM_MINMAX, CV_64F);
    cv::Mat image = cv::Mat(160,160,CV_8UC3);

	//change these values if needed to suit your needs
	float thrsh = 0.25;
	float i_gamma = 2.5;
	int corner = 3;
	int i_1,j_1;

	cv::pow(img_gray, i_gamma,img);
	double data[3][3] = {{-0.1,-0.1,-0.1},{-0.1,2.5,-0.1},{-0.1,-0.1,-0.1}};
	cv::Mat filt = cv::Mat(3,3, CV_64F, data);
	cv::filter2D(img , temp_filt, -1, filt);
	
	for(i=0;i<16;i++){
		for(j=0;j<16;j++){
			if((i+j)<(31-corner)){
				if(temp_filt.at<double>(i,j) <= thrsh)
					temp_bw.at<double>(i,j) =1;
				else 
					temp_bw.at<double>(i,j) = 0;
			}
			else{	i_1 = abs(i-15);
				j_1 = abs(j-15);
				temp_bw.at<double>(i_1,j_1) = 0;
				temp_bw.at<double>(15-i_1,j_1) = 0;
				temp_bw.at<double>(i_1,15-j_1) = 0;
			}
		}
	}
	temp_fin = temp_bw.mul(img_1);
	float vegtemp = sum(temp_fin)[0]/sum(temp_bw)[0];			
	cv::resize(img_gray, img_gray, cv::Size(), 10, 10);
	image = cv::Scalar(0,255,255);
	for(i=0;i<160;i++){
		for(j=0;j<160;j++){
			image.at<cv::Vec3b>(i,j)[1] = image.at<cv::Vec3b>(i,j)[1]*(1-img_gray.at<double>(i,j));
		}
	}
    cv::imshow("Display Image", image);
    cv::waitKey(1);

	return vegtemp;
}


